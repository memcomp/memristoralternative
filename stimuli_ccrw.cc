#include "systemc.h"
#include "stimuli_ccrw.h"

void stimuli_ccrw::read(){
	cout << "MemValue: " << sd_digit_out << endl;
//	cout << "-----------------------------" << endl;
}



/*
	sc_out<bool>		read_enable;
	sc_out<bool>		write_enable;
	sc_out<bool>		in_plus;
	sc_out<bool>		in_minus;
	sc_in<sd_digit>		sd_digit_out;
*/

/*	Was wir testen müssen

	Pegel hochgehen/runtergehen beim lesen 
	Pegel passt wieder nach beiden aufrufen

	Fehler (read && write) || (in+ && in-)
*/
bool readVals[] = 	{true, 	true,	true,	true,	false,	true,	true,	false};
bool writeVals[] = 	{true, 	false, 	false,	false, 	true,	false,	false,	false};	
bool in_plusVals[] = 	{true, 	true, 	true,	false,	false,	true,	false,	false};
bool in_minusVals[] = 	{false,	true, 	false,	true,	false,	false,	true,	false};



void stimuli_ccrw::write(){
	cout << "Test for errors"<<endl;
	cout << "(read,\twrite,\tin+,\tin-)" << endl;
	cout << "---------------" << endl;
	
	for(int i = 0; i < sizeof(readVals)/sizeof(bool); i++){
		cout << "(" << readVals[i] << ",\t" << writeVals[i] <<",\t"<< in_plusVals[i] << ",\t" << in_minusVals[i] << ")" << endl; 

		read_enable = readVals[i];
		write_enable = writeVals[i];
		in_plus = in_plusVals[i];
		in_minus = in_minusVals[i];
		
		wait(); read();
		
		read_enable = false;
		write_enable = false;
		in_plus = false;
		in_minus = false;

		//wait(); read();
			
		cout << "---------------" << endl;
	}
}

