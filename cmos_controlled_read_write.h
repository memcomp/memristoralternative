#include "systemc.h"
#include "memristorAlt.h"


SC_MODULE(ccrw){
	sc_in<bool>		read_enable;
	sc_in<bool>		write_enable;
	
	sc_in<bool>		in_plus;
	sc_in<bool>		in_minus;
	
	sc_out<sd_digit>	sd_digit_out;
	sc_in<bool>		clock;

	sc_signal<bool> 	in_plus_sig;
	sc_signal<bool> 	in_minus_sig;


	memristorAlt *myMemristor;
	void write();

	SC_CTOR(ccrw){
		myMemristor = new memristorAlt("memristor");

		myMemristor->positive_in(in_plus_sig);	
		myMemristor->negative_in(in_minus_sig);
		myMemristor->sd_digit_out(sd_digit_out);
		myMemristor->clock(clock);
		

		SC_METHOD(write);
		sensitive << read_enable << write_enable << in_plus << in_minus;
	};
};
