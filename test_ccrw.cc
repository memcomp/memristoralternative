#include "test_ccrw.h"

int sc_main(int, char *[]){
test_ccrw *ccrw_inst ;
ccrw_inst = new test_ccrw("CCRW_TEST") ;

sc_clock clock("clk", 1, SC_NS) ;

ccrw_inst->clock(clock) ;

sc_start(20, SC_NS) ;
return 0;
}
