#include "systemc.h"
#include "common.h"

SC_MODULE(memristorAlt){
	sc_in<bool>		positive_in;
	sc_in<bool>		negative_in;
	sc_out<sd_digit>	sd_digit_out;
	sc_in<bool>			clock;

	sd_digit myValue;

	void read();
	sd_digit calcSD_Digit(sd_digit current, bool up);
	void write();

	SC_CTOR(memristorAlt){
		SC_METHOD(read);
		sensitive << clock.pos();

		SC_METHOD(write);
		sensitive << clock.neg();
	};
};
