#include "memristorAlt.h"

//liest ausgaenge schreibt internen zustand
void memristorAlt::read() {
	cout << "reading" << endl;
	bool pos = positive_in.read();
	bool neg = negative_in.read();

	if(pos) sd_digit_out = myValue;
}

void memristorAlt::write(){
	cout << "writing" << endl;
	bool pos = positive_in.read();
	bool neg = negative_in.read();

	if(pos || neg) myValue = calcSD_Digit(myValue, pos);
}


sd_digit memristorAlt::calcSD_Digit(sd_digit current, bool up){
	if(up){
		if(current == ZERO) return ONE;
		if(current == ONE) return MINUS_ONE;
		return MINUS_ONE;
	} else {
		if(current == ONE) return ZERO;
		if(current == MINUS_ONE) return ONE;
		return ZERO;
	}
}
