#include "systemc.h"
#include "cmos_controlled_read_write.h"
#include "stimuli_ccrw.h"

SC_MODULE(test_ccrw){
	sc_signal<bool>		read_enable;
	sc_signal<bool>		write_enable;
	
	sc_signal<bool>		in_plus;
	sc_signal<bool>		in_minus;
	
	sc_signal<sd_digit>	sd_digit_out;
	sc_in<bool>		clock;

	ccrw *ccrw_inst;
	stimuli_ccrw *stimuli_ccrw_inst;


	SC_CTOR(test_ccrw){
		ccrw_inst = new ccrw("ccrw");

		ccrw_inst->read_enable(read_enable);
		ccrw_inst->write_enable(write_enable);
		ccrw_inst->in_plus(in_plus);
		ccrw_inst->in_minus(in_minus);
		ccrw_inst->sd_digit_out(sd_digit_out);
		ccrw_inst->clock(clock);

		stimuli_ccrw_inst = new stimuli_ccrw("stimuli_ccrw");
		
		stimuli_ccrw_inst->read_enable(read_enable);
		stimuli_ccrw_inst->write_enable(write_enable);
		stimuli_ccrw_inst->in_plus(in_plus);
		stimuli_ccrw_inst->in_minus(in_minus);
		stimuli_ccrw_inst->sd_digit_out(sd_digit_out);
		stimuli_ccrw_inst->clock(clock);
		


	};
};
