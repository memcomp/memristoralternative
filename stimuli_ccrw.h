#include "systemc.h"
#include "common.h"

SC_MODULE(stimuli_ccrw){
	sc_out<bool>		read_enable;
	sc_out<bool>		write_enable;
	
	sc_out<bool>		in_plus;
	sc_out<bool>		in_minus;
	
	sc_in<sd_digit>		sd_digit_out;
	sc_in<bool>		clock;

	void write();
	void read();
	SC_CTOR(stimuli_ccrw){

		SC_THREAD(write);
		sensitive << clock.pos();

		SC_THREAD(read);
		sensitive << sd_digit_out << clock.pos();

	};
};
