#include "cmos_controlled_read_write.h"

/*
void memristorAlt::read() {
	sd_digit_out = myValue;
}*/

//liest ausgaenge schreibt internen zustand
void ccrw::write(){
	
	bool read = read_enable.read();
	bool write = write_enable.read();	
	bool pos = in_plus.read();
	bool neg = in_minus.read();
	if(read && write) { //FEHLER
		cout << "read_enable und write_enable true" << endl;
		return;
	}

	if(pos && neg) {
		cout << "pos und neg gleichzeitig" << endl;
		return;
	}

	cout << "MyValue vorher: " << myMemristor->myValue << endl;

	if(read){ //linke seite
	//	if(pos && neg) readBefore = true;
	//	if(readBefore) {
			in_plus_sig.write(pos);
			in_minus_sig.write(neg);
	//	} else {
	//		in_plus_sig.write(false);
	//		in_plus_sig.write(false);
	//	}
	} else if(write){ //rechte seite, 
		in_plus_sig.write(pos);
		in_minus_sig.write(neg);
	} else {
		in_plus_sig.write(false);
		in_minus_sig.write(false);
	}

	cout << "MyValue danach: " << myMemristor->myValue << endl;
}
